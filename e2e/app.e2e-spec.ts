import { AnsAfriquePage } from './app.po';

describe('ans-afrique App', function() {
  let page: AnsAfriquePage;

  beforeEach(() => {
    page = new AnsAfriquePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
