import * as keystone from 'keystone'
var Types = keystone.Field.Types

/**
 * Post Model
 * ==========
 */

var Post = new keystone.List('Post', {
	map: { name: 'title' },
	autokey: { path: 'slug', from: 'title', unique: true },
  label: 'Articles',
  plural: 'Articles',
  singular: 'Article'
})

Post.add({
  title: { type: String, required: true, label: 'titre' },
  author: { type: Types.Relationship, ref: 'User', index: true, label: 'Auteur' },
  publishedDate: { type: Types.Date, index: true, dependsOn: { state: 'published' }, label: 'Date de publication'},
  image: { type: String/*Types.CloudinaryImage*/ },
  content: {
    brief: { type: Types.Html, wysiwyg: true, height: 150, label: 'Résumé'},
    extended: { type: Types.Html, wysiwyg: true, height: 400, label: 'Contenu complet'},
  },
  categories: { type: Types.Relationship, ref: 'PostCategory', many: true },
  state: {
    type: Types.Select,
    label: 'Status',
    options: [
      {value: 'draft', label: 'Brouillon'},
      {value: 'published', label: 'Publié'},
      {value: 'archived', label: 'Archivé'}
    ],
    default: 'draft', index: true
  },
})

Post.schema.virtual('content.full').get(function () {
  return this.content.extended || this.content.brief
})

Post.defaultColumns = 'title, state|20%, author|20%, publishedDate|20%'
Post.register()
