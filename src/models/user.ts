//var keystone = require('keystone'),
import * as keystone from 'keystone'

let Types = keystone.Field.Types;
 
var User = new keystone.List('User', {
  label: 'Utilisateurs',
  plural: 'Utilisateurs',
  singular: 'Utilisateur'
});

User.add({
    name: { type: Types.Name, required: true, index: true, label: 'nom' },
    email: { type: Types.Email, initial: true, required: true, index: true },
    password: { type: Types.Password, initial: true, label: 'mot de passe' },
    canAccessKeystone: { type: Boolean, initial: true, label: 'a accès à l\'interface d\'adminisatration' }
});

User.register();



