var keystone = require('keystone');

/**
 * PostCategory Model
 * ==================
 */

var PostCategory = new keystone.List('PostCategory', {
	autokey: { from: 'name', path: 'key', unique: true },
  label: 'Categories d\'articles',
  plural: 'Categories',
  singular: 'Categorie'
});

PostCategory.add({
	name: { type: String, required: true , label: 'nom'},
});

PostCategory.relationship({ ref: 'Post', path: 'categories' });

PostCategory.register();