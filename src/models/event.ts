import * as keystone  from 'keystone'

let Types = keystone.Field.Types


let Event = new keystone.List('Event', {
  //map: {name: 'title'},
  //autokey:  {path: 'slug', from: 'title', unique: true }
  label: 'Evènement',
  plural: 'Evènement',
  singular: 'Evènement'
})
Event.add({
  name: {
    type: String,
    required: true,
    label: 'Titre',
  },
  date: {
    type: Date,
    default: Date.now,
    required: true,
    lablel: 'Date et heure',
    format: 'DD-MM-YYYY HH:mm'
  },
  place: {
    type: String,
    default: "",
    lablel: 'Lieu'
  },
  flyer: {
    type: String,
    default: "",
    lablel: 'Affiche'
  },/*,
  time: {
    type: String,
    default:
    required: true,
  },*/
  content: {
    type: Types.Html,
    // required: true,
    default: "",
    lablel: 'Description',
    wysiwyg: true
  },
  type: {
    type: Types.Select,
    label: 'Type d\'évènement',
    options: [
      {value: 'incoming', label: 'A venir'},
      {value: 'old', label: 'Passé'},
    ],
    required: true,
    default: 'incoming'
  },
  /*
  createdBy
  updatedBy
  */
  pictures: {
    type: Types.TextArray,// or replace it with CloudinaryImages
    dependsOn: {type: "old"},
    defaut: null
  },
  status: {
    type: Types.Select,
    // required: true,
    options: [
      {value: 'draft', label: 'Brouillon'},
      {value: 'published', label: 'Publié'}
    ],
    default: 'draft'
  },
  lastUpdate: {
    type: Types.Datetime,
    default: Date.now,
    hidden: true,
    label: "Mise à jour"
  }
})

// options
Event.defaultSort = "date"
Event.defaultColumns = "name, place, date, type, status, lastUpdate"

Event.register()