import * as keystone from 'keystone'

let Types = keystone.Field.Types

let Gallery = keystone.List('Gallery', {
  label: 'Galleries',
  plural: 'Galleries',
  singular: 'Gallerie'
})

Gallery.add({
  name: {
    type: String,
    required: true,
    label: 'Titre'
  },
  images: {
    type: Types.TextArray,
    default: null
  },
  status: {
    type: Types.Select,
    // required: true,
    options: [
      {value: 'draft', label: 'Brouillon'},
      {value: 'published', label: 'Publié'}
    ],
    default: 'draft'
  },
  lastUpdate: {
    type: Types.Datetime,
    default: Date.now,
    hidden: true,
    label: "Mise à jour"
  },
  /*
  createdBy
  updatedBy
  */
})

Gallery.defaultSort = "name"
Gallery.defaultColumns = "name, status, lastUpdate"
Gallery.register()