import * as keystone from 'keystone'

const User = keystone.list('User')

export = function (done) {
  const admin = new User.model({
    email: 'mo.assiongbon@gmail.com',
    password: 'keystone',
    name: { first: 'Admin', last: 'User' }
  })

  admin.isAdmin = true

  admin.save()
    .then(() => done())
    .catch(err => done(err))
}