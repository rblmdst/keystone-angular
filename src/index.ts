//var keystone = require('keystone');
import * as keystone  from 'keystone'

import { setRoutes } from './routes'

//require('./models');
//import * as models from './models'
keystone.import('models');

//import { User } from './models/user';

// User.register()

keystone.init({
  
  'name': 'ans-afrique',
  
  //'favicon': 'public/favicon.ico',
  //'less': 'public',
  'static': ['public'],
  
  //'views': 'templates/views',
  //'view engine': 'jade',
  
  'auto update': true,
  'mongo': 'mongodb://localhost/keystone-angular',
  
  'session': true,
  'auth': true,
  'user model': 'User',
  'cookie secret': 'Have	no	fear	of	perfection	-	you’ll	never	reach	it.'
  
});
 
 
//keystone.set('routes', require('./routes'));

keystone.set('routes', setRoutes);
// Configure the navigation bar in Keystone's Admin UI
keystone.set('nav', {
  // label : modelNamePluaral
  utilisateurs: 'User',
  articles: 'Post',
  "Categories d'article": 'PostCategory',
  evenements: 'Event',
  galleries: 'Gallery'
})
 
keystone.start();